#include "findIndex.h"

const char sc_lower[] = {
	'q',
	'w',
	'e',
	'r',
	't',
	'y',
	'u',
	'i',
	'o',
	'p',
	'a',
	's',
	'd',
	'f',
	'g',
	'h',
	'j',
	'k',
	'l',
	'z',
	'x',
	'c',
	'v',
	'b',
	'n',
	'm',
};

const char sc_upper[] = {
	'Q',
	'W',
	'E',
	'R',
	'T',
	'Y',
	'U',
	'I',
	'O',
	'P',
	'A',
	'S',
	'D',
	'F',
	'G',
	'H',
	'J',
	'K',
	'L',
	'Z',
	'X',
	'C',
	'V',
	'B',
	'N',
	'M',	
};

char toUpper(char c) {
	int index = findIndexChar(sc_lower, 25, c);
	if(index != -1)
	{
		return sc_upper[index];
	}
	else
	{
		return c;
	}
}

char toLower(char c) {
	int index = findIndexChar(sc_upper, 25, c);
	if(index != -1) 
	{
		return sc_lower[index];
	}
	else 
	{
		return c;
	}
}
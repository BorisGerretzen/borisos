// Ghetto memcopy solution
void memcopy(char* source, char* dest, int numBytes) {
	int i;
	for(i = 0; i < numBytes; i++) {
		*(dest + i) = *(source + i);
	}
}
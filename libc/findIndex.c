int findIndexChar( const char a[], int size, char value )
{
    int index = 0;
    while (index < size && a[index] != value) ++index;
    return (index == size ? -1 : index);
}
#ifndef ITOA_H
#define ITOA_H
#include "../cpu/types.h"
#include "stdio.h"
void itoa(int n, char str[]);
#endif
#ifndef STRING_H
#define STRING_H

#include "append.h"
#include "backspace.h"
#include "itoa.h"
#include "reverse.h"
#include "strcmp.h"
#include "strlen.h"

#endif
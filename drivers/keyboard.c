#include "keyboard.h"
#include "ports.h"
#include "../cpu/isr.h"
#include "screen.h"
#include "../kernel/kernel.h"
#include "../libc/case.h"

void print_letter(u8 scancode);

#define BACKSPACE 0x0E
#define ENTER 0x1C
#define LSHIFT 0x2A
#define LSHIFT_RELEASE 0xAA
#define CAPS 0x3A
#define SC_MAX 57
static char key_buffer[256];

const char *sc_name[] = { 
    "ERROR", 
    "Esc", 
    "1", 
    "2",
    "3", 
    "4", 
    "5", 
    "6", 
    "7", 
    "8", 
    "9", 
    "0", 
    "-", 
    "=", 
    "Backspace", 
    "Tab",
    "Q", 
    "W", 
    "E", 
    "R", 
    "T", 
    "Y", 
    "U", 
    "I", 
    "O", 
    "P", 
    "[", 
    "]", 
    "Enter", 
    "Lctrl", 
    "A", 
    "S", 
    "D", 
    "F", 
    "G", 
    "H", 
    "J", 
    "K", 
    "L", 
    ";", 
    "'", 
    "`", 
    "LShift", 
    "\\", 
    "Z", 
    "X", 
    "C", 
    "V", 
    "B", 
    "N", 
    "M", 
    ",", 
    ".", 
    "/", 
    "RShift", 
    "Keypad *", 
    "LAlt", 
    "Spacebar"};

const char sc_ascii[] = {
    '\0',
    '\0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '0',
    '-',
    '=',
    '\0',
    '\0',
    'q',
    'w',
    'e',
    'r',
    't',
    'y',
    'u',
    'i',
    'o',
    'p',
    '[',
    ']',
    '\0',
    '\0',
    'a',
    's',
    'd',
    'f',
    'g',
    'h',
    'j',
    'k',
    'l',
    ';',
    '\'',
    '`',
    '\0',
    '\\',
    'z',
    'x',
    'c',
    'v',
    'b',
    'n',
    'm',
    ',',
    '.',
    '/',
    '\0',
    '*',
    '\0',
    ' '};

const char sc_ascii_shifted[] = {
    '\0',
    '\0',
    '!',
    '@',
    '#',
    '$',
    '%',
    '^',
    '&',
    '*',
    '(',
    ')',
    '_',
    '+',
    '\0',
    '\0',
    'Q',
    'W',
    'E',
    'R',
    'T',
    'Y',
    'U',
    'I',
    'O',
    'P',
    '{',
    '}',
    '?',
    '?',
    'A',
    'S',
    'D',
    'F',
    'G',
    'H',
    'J',
    'K',
    'L',
    ':',
    '\"',
    '~',
    '\0',
    '|',
    'Z',
    'X',
    'C',
    'V',
    'B',
    'N',
    'M',
    '<',
    '>',
    '?',
    '\0',
    '*',
    '\0',
    ' '};

int shifting;
int caps;
static void keyboard_callback(registers_t regs) {
	// Get scancode in from port 0x60
	u8 scancode = port_byte_in(0x60);
	if (scancode == BACKSPACE) {
        backspace(key_buffer);
        kprint_backspace();
        return;
    } else if (scancode == ENTER) {
        kprint("\n");
        user_input(key_buffer); 
        key_buffer[0] = '\0';
        return;
    } else if(scancode == LSHIFT) {
    	shifting = 1;
    }
    else if(scancode == LSHIFT_RELEASE) {
    	shifting = 0;
    } else if(scancode == CAPS) {
    	caps = (caps == 1) ? 0 : 1;
    }
    if(scancode > SC_MAX) return;
    char letter;
    if(shifting == 1 && caps == 1) 			letter = toLower(sc_ascii_shifted[(int) scancode]);	// Shift + caps = normal letters + special symbols
    else if(shifting == 1) 					letter = sc_ascii_shifted[(int) scancode];			// Just shift = uppercase letters + special symbols
    else if(caps == 1)						letter = toUpper(sc_ascii[(int) scancode]);			// Caps = uppercase letters + normal symbols
    else 									letter = sc_ascii[(int) scancode];					// Nothing = normal letters + normal symbols
    char str[2] = {letter, '\0'}; // kprint only accepts char[]
    append(key_buffer, letter);
    kprint(str);
}

void init_keyboard() {
   register_interrupt_handler(IRQ1, keyboard_callback); 
}
#include "timer.h"
#include "../drivers/screen.h"
#include "../libc/stdio.h"
#include "isr.h"
#include "../drivers/ports.h"

u32 tick = 0;

static void timer_callback(registers_t regs)
{
	tick++;
	// kprint("Tick: ");
	char tick_ascii[256];
	itoa(tick, tick_ascii);
	// kprint(tick_ascii);
	// kprint("\n");
}

void init_timer(u32 freq) 
{
	register_interrupt_handler(IRQ0, timer_callback);

 	// Get PIT value
 	u32 divisor = 1193180 / freq;	// 1193180hz is oscillator frequency
 	u8 low = (u8)(divisor & 0xFF);
 	u8 high = (u8)( (divisor >> 8) & 0xFF);
 	
    port_byte_out(0x43, 0x36); /* Command port */
    port_byte_out(0x40, low);
    port_byte_out(0x40, high);
}
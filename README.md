# README #

## How do I get set up? ##
### Creating a cross compiler ###
**Requirements:**

 * GCC (existing release you wish to replace)
 * Binutils
 * G++ (if building a version of GCC >= 4.8.0)
 * GNU Make
 * GNU Bison
 * Flex
 * GNU GMP
 * GNU MPFR
 * GNU MPC
 * Texinfo


**Preparation:**

$PREFIX will be the installation directory of your compiler, change it to wherever you want.
```
#!bash
export PREFIX="$HOME/opt/cross"
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"
```

**Binutils:**

This compiles the binutils (assembler, disassembler, linker, etc) required for building the OS.
```
#!bash
cd $HOME/src
mkdir build-binutils
cd build-binutils
../binutils-x.y.z/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make
make install
```

**GCC:**

We are not building everything of GCC because a lot is unnecessary for what we're doing.
```
#!bash
cd $HOME/src
 
# The $PREFIX/bin dir _must_ be in the PATH. We did that above.
which -- $TARGET-as || echo $TARGET-as is not in the PATH
 
mkdir build-gcc
cd build-gcc
../gcc-x.y.z/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc
```

**Using the compiler:**

You can compile with the compiler by using the following command or by adding it to you path:
```
#!bash

$HOME/opt/cross/bin/i686-elf-gcc --version
```


###Building the OS:###

**Requirements:**

 * NASM
 * Your crosscompiler
 * Binutils
 * Make

**Building:**

To create the os binary run the following command:

```
#!bash

make
```
This will automatically assemble the assembly files, build the object files and compile them into the finished os binary.

To create and start the binary in qemu run:

```
#!bash

make run
```

Please make sure that you actually have qemu installed before doing this.
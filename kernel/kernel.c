#include "../drivers/screen.h"
#include "../drivers/keyboard.h"
#include "../libc/stdio.h"
#include "../cpu/isr.h"
#include "../cpu/idt.h"
#include "../cpu/timer.h"

void main()
{
	clear_screen();

	// Setup irq and isr
	isr_install();
	asm volatile("sti");
	init_timer(50);
	init_keyboard();


	kprint_center("-= Welkom bij Windhoos 1 =-\n", 2);
	kprint_center("Type END to halt cpu:\n", -1);
	kprint(">");
}

void user_input(char *input) {
    if (strcmp(input, "END") == 0) {
        kprint("Halting CPU!\n");
        asm volatile("hlt");
    }
    kprint("You said: ");
    kprint(input);
    kprint("\n> ");
}
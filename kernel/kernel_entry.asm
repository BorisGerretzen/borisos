; Always start main method in kernel
[bits 32]				; 32 bits
[extern main]			; Declare external method main
call main				; Call that method
jmp $					; Never return
C_SOURCES = $(wildcard kernel/*.c drivers/*.c libc/*.c cpu/*.c)
HEADERS = $(wildcard kernel/*.h drivers/*.h libc/*.h cpu/*.h)

OBJ = ${C_SOURCES:.c=.o cpu/interrupt.o}

all: os-image

run: all
	qemu-system-i386 -fda os-image.bin

os-image: boot/boot_sect.bin kernel.bin
	cat $^ > os-image.bin

kernel.bin: kernel/kernel_entry.o ${OBJ}
	ld -o $@ -Ttext 0x1000 $^ --oformat binary -m elf_i386

%.o : %.asm
	nasm $< -f elf -o $@

%.o : %.c ${HEADERS}
	i686-elf-gcc -ffreestanding -c $< -o $@

%.bin : %.asm
	nasm $< -f bin -I ' ../../16bit/' -o $@

clean:
	rm -fr *.bin *.dis *.o os-image
	rm -fr kernel/*.o boot/*.bin drivers/*.o libc/*.o cpu/*.o

git: clean
	git add -A


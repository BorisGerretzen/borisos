print_string:		; Output string in SI to screen
	mov ah, 0Eh		; bios teletype output

.repeat:
	lodsb			; Get character from string
	cmp al, 0
	je .done		; If char is zero, end of string
	int 10h			; Otherwise print 
	jmp .repeat		; Recursion ftw

.done:
	ret
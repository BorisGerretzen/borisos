; LOAD DH sectors to ES:BX from drive DL

disk_load:
	push dx				; Store DX on stack so we can check if all sectors have been read

	mov ah, 0x02		; Bios read sector function
	mov al, dh
	mov ch, 0x00		; Cylinder 0
	mov dh, 0x00		; Head 0
	mov cl, 0x02		; Start read from sector 2, aka after boot sector

	int 0x13			; Interrupt

	jc disk_error

	pop dx 				; Restore dk from stack
	cmp dh, al 			; if sectors read != sectors expected
	jne disk_error
	ret

disk_error:
	mov si, DISK_ERROR_MSG
	call print_string
	jmp $

; Variables
DISK_ERROR_MSG			db "Disk read error!", 0
[bits 16]

; Switching to protected mode
switch_to_pm:
	cli						; Switch off interrupts until protected mode has been started

	lgdt [gdt_descriptor]	; Load global descriptor table

	mov eax, cr0			; For making switch to prot mode, set cr0's first bit 
	or eax, 0x1				; CR0 does not want to be edited so well just do it  like this
	mov cr0, eax			; Fucking CR0

	jmp CODE_SEG:init_pm	; Make far jump to the 32 bit code. Forces CPU to flush cache
							; CPU cache might contain prefetched real mode instructions and that would fuck up on 32 bit

[bits 32]
; Init registers and stack to protected mode

init_pm:
	mov ax, DATA_SEG		; Old segments are worthless so point registers to data selector from GDT.
	mov ds, ax
	mov ss, ax
	mov es, ax
	mov fs, ax
	mov fs, ax

	mov ebp, 0x90000		; Update stack so its on top of free space
	mov esp, ebp

	call BEGIN_PM
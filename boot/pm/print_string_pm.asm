[bits 32]
; constants
VIDEO_MEMORY equ 0xb8000 		; Start of video memory
WHITE_ON_BLACK equ 0x0f			; Don't want none of them disco colors

print_string_pm:
	pusha						; Push registers to stack
	mov edx, VIDEO_MEMORY		; Set edx to video memory start

print_string_pm_loop:
	mov al, [ebx]				; Store first char at ebx in al
	mov ah, WHITE_ON_BLACK		; Set colors

	cmp al, 0					; if (al == 0), at end of string,
	je print_string_pm_done		; jump to done

	mov [edx], ax				; store char and attributes at character cell

	add ebx, 1					; increase ebx to next char
	add edx, 2					; move to next cell in vid mem, increment by 2, each cell is 2 bytes

	jmp print_string_pm_loop	; Next char incoming

print_string_pm_done:
	popa						; Pop all registers from stack
	ret 						; And return
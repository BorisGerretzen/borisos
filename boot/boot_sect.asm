; Boot sector that boots C kernel in 32-bit prot mode

[org 0x7c00]
KERNEL_OFFSET equ 0x1000  ; Kernel memory offset

mov [BOOT_DRIVE], dl      ; BIOS stores boot drive in DL

mov bp, 0x9000            ; Setup for stack
mov sp, bp

mov si, MSG_REAL_MODE     ; Announce real mode start
call print_string

call load_kernel          ; Load kernel

call switch_to_pm         ; Make the switch to protected mode, We wont return from this if everything goes ok

jmp $                     ; Infinite loop just to be sure

; Include routines
%include "boot/print/print_string.asm" ; Print string 
%include "boot/disk/disk_load.asm"     ; Disk load
%include "boot/pm/gdt.asm"             ; Global descriptor table
%include "boot/pm/print_string_pm.asm" ; Print string without bios interrupt calls
%include "boot/pm/switch_to_pm.asm"    ; For making the switch to protected mode

[bits 16]

; load_kernel
load_kernel:
  mov si, MSG_LOAD_KERNEL ; Print message while loading kernel, for debugging purposes
  call print_string

  mov bx, KERNEL_OFFSET   ; Setup disk_load, make sure we load 15 sectors from BOOT_DRIVE into memory at KERNEL_OFFSET
  mov dh, 15              ; 15 sectors
  mov dl, [BOOT_DRIVE]    ; Boot drive
  call disk_load          ; Load that shit

  ret

[bits 32]
; Welcome to 32-bit land

BEGIN_PM:
  mov ebx, MSG_PROT_MODE  ; Announce that we've reached 32 bit mode
  call print_string_pm

  call KERNEL_OFFSET      ; Jump to the loaded kernel code

  jmp $                   ; Make sure we dont return

; Globals
BOOT_DRIVE        db 0
MSG_REAL_MODE     db "Started in 16-bit real mode.", 0
MSG_PROT_MODE     db "Landed in 32-bit prot mode.", 0
MSG_LOAD_KERNEL   db "Loading kernel to memory.", 0

times 510-($-$$) db 0     ; Pad to 512-2=510 bytes
dw 0xaa55                 ; Magic number
